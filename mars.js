
var MarsRover = function (location = [0,0], direction = 'N', canvas = [10,10], obstacles = []) {
	// ^ I really wanted to try the "new" default params, this won't work with older browsers
	var self = this; //sets the classic scope

	this.location = location;
	this.direction = direction;
	this.canvas = canvas;
	this.obstacles = obstacles;

	this.executeCommand = function(commands = []) {
		var cmdsLength = commands.length;
		for(var i = 0; i < cmdsLength; i++) {
			var command = commands[i];

			if (command === 'f' || command === 'b') {
				move(command);
			}else if(command === 'l' || command === 'r') {
				setDirection(command);
			}
		}
	}

	function move (command) {
		var moveX = 0, moveY = 0;

		if (self.direction === 'N' ) {
			moveY = 1;
		} else if (self.direction === 'S') {
			moveY = -1;
		} else if (self.direction === 'E') {
			moveX = 1;
		} else if (self.direction === 'W') {
			moveX = -1;
		}


		//reverse movement since command is backwards
		if(command === 'b') {
			moveX *= -1;
			moveY *= -1;
		}
		var futureLocation = [self.location[0] + moveX , self.location[1] + moveY];
		if (isObstacle(futureLocation)) {
			console.log('Obstacle!'); //Console log is the official mars rover broadcasting method
		}else {
			self.location = warpGrid(futureLocation);
			console.log('New location: ' + self.location);
		}
	}

	function setDirection (command) {
		var directionList = ['N', 'E', 'S', 'W'];

		var currentPosition = directionList.indexOf(self.direction);

		if (command === 'l') {
			currentPosition -= 1;
		}else if(command === 'r') {
			currentPosition +=1;
		}
		
		// I'm sure there is a better way, can't think of one for some reason
		if (currentPosition === 4) {
			currentPosition = 0;
		} else if (currentPosition === -1) {
			currentPosition =3;
		}

		self.direction = directionList[currentPosition];
	}

	function isObstacle (futureLocation) { 
		var obstacleLength = self.obstacles.length;
		for(var i = 0; i < obstacleLength; i++) {

			//I'm being lazy here, .toString will not check type of values, should be fine for this use case :/
			if(futureLocation.toString() === self.obstacles[i].toString()) { 
				return true;
			}
		}
		return false;
	}

	function warpGrid(futureLocation) {
		// X warp
		if(futureLocation[0] > self.canvas[0]) {
			futureLocation[0] = 0;
		}
		if(futureLocation[0] < 0) {
			futureLocation[0] = self.canvas[0];
		}

		// Y warp
		if(futureLocation[1] > self.canvas[1]) {
			futureLocation[1] = 0;
		}
		if(futureLocation[1] < 0) {
			futureLocation[1] = self.canvas[1];
		}

		return futureLocation;
	}

}


//debugging / testing

var rover = new MarsRover(undefined, 'N', undefined, [[1,0]]);
rover.executeCommand(['f','f']);